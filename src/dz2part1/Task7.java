package dz2part1;

import java.util.Scanner;

/*
На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов, отсортированный по возрастанию.
Необходимо создать массив, полученный из исходного возведением в квадрат
каждого элемента, упорядочить элементы по возрастанию и вывести их на
экран.
Ограничения:
● 0 < N < 100
● -1000 < ai < 1000
 */
public class Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = (int) Math.pow(scanner.nextInt(), 2);
        }
        selectionSort(arr);
        for (int element: arr) {
            System.out.print(element + " ");
        }
    }

    public static void selectionSort(int[] list) {
        
        for (int i = 0; i < list.length - 1; i++) {
            int minValue = list[i];
            int minIndex = i;
            for (int j = i + 1; j < list.length; j++) {
                if(list[j] < minValue){
                    minValue = list[j];
                    minIndex = j;
                }
            }
            if(minIndex != i){
                list[minIndex] = list[i];
                list[i] = minValue;
            }
        }
    }
}
