package dz2part1;
/*
На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов. После этого передается число M — величина
сдвига.
Необходимо циклически сдвинуть элементы массива на M элементов вправо.
Ограничения:
● 0 < N < 100
● -1000 < ai < 1000
● 0 <= M < 100
 */

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
        }
        int m = scanner.nextInt();
        for (int i = 0; i < m; i++) {
            shiftArrayToRightByOneNumber(arr);
        }
        for (int element: arr) {
            System.out.print(element + " ");
        }
    }

    private static void shiftArrayToRightByOneNumber(int[] arr) {
        int temp = arr[arr.length - 1];
        for (int i = arr.length -1; i > 0; i--) {
            arr[i] = arr[i - 1];
        }
        arr[0] = temp;
    }

}
