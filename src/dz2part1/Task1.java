package dz2part1;
/*
На вход подается число N — длина массива. Затем передается массив
вещественных чисел (ai) из N элементов.
Необходимо реализовать метод, который принимает на вход полученный
массив и возвращает среднее арифметическое всех чисел массива.
Вывести среднее арифметическое на экран.
Ограничения:
● 0 < N < 100
● 0 < ai < 1000
 */

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        double[] arr = new double[n];
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextDouble();
        }
        System.out.println(arithmeticMean(arr));
    }
    public static double arithmeticMean(double[] arr){
        double sum = 0;
        for (double element: arr) {
            sum += element;
        }
        return sum / arr.length;
    }
}
