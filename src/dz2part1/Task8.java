package dz2part1;
/*
 На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов. После этого передается число M.
Необходимо найти в массиве число, максимально близкое к M (т.е. такое число,
для которого |ai - M| минимальное). Если их несколько, то вывести
максимальное число.
Ограничения:

● 0 < N < 100
● -1000 < ai < 1000
● -1000 < M < 1000
 */

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
        }
        int m = scanner.nextInt();
        int minValue = arr[0];

        for (int i = 1; i < n; i++) {
            int i1 = Math.abs(Math.abs(arr[i]) - Math.abs(m));
            if (i1 < Math.abs(Math.abs(minValue) - Math.abs(m))){
                minValue = arr[i];
            } else if(i1 == Math.abs(Math.abs(minValue) - Math.abs(m))){
                if(arr[i] > minValue){
                    minValue = arr[i];
                }
            }
        }
        System.out.println(minValue);
    }
}
