package dz2part1;

import java.util.Scanner;

/*
На вход подается число N — длина массива. Затем передается массив
строк из N элементов (разделение через перевод строки). Каждая строка
содержит только строчные символы латинского алфавита.
Необходимо найти и вывести дубликат на экран. Гарантируется что он есть и
только один.
Ограничения:
● 0 < N < 100
● 0 < ai.length() < 1000
 */
public class Task9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        String[] strings = new String[n];
        for (int i = 0; i < n; i++) {
            strings[i] = scanner.next();
        }
        findDoubleString(strings);

    }

    private static void findDoubleString(String[] strings) {
        String doubleS = strings[0];
        for (int i = 1; i < strings.length; i++) {
            for (int j = i; j < strings.length; j++) {
                if(doubleS.equals(strings[j])){
                    System.out.println(doubleS);
                    return;
                }
            }
            doubleS = strings[i];
        }
    }
}

