package dz2part1;

import java.util.Scanner;

/*
На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов, отсортированный по возрастанию.
Необходимо вывести на экран построчно сколько встретилось различных
элементов. Каждая строка должна содержать количество элементов и сам
элемент через пробел.

Ограничения:
● 0 < N < 100
● -1000 < ai < 1000
 */
public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] arr = new int[n];
        int[][] arr2 = new int[2][n];
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
        }
        arr2[0][0] = 1;
        arr2[1][0] = arr[0];
        int arrIndex = 1;
        int arr2Index = 0;
        while (arrIndex < arr.length){
            int inIndex = findFirstIndexValueInArray(arr,0,arrIndex,arr[arrIndex]);
            if (inIndex >= 0){
                arr2[0][findFirstIndexValueInArray(arr2[1],0, n,arr[arrIndex])]++;
            } else{
                arr2Index++;
                arr2[0][arr2Index] = 1;
                arr2[1][arr2Index] = arr[arrIndex];
            }
            arrIndex++;
        }
        for (int i = 0; i < arr2[1].length; i++) {
            if (arr2[0][i] == 0){
                break;
            }
            System.out.println(arr2[0][i] + " " + arr2[1][i]);
        }
    }
    public static int findFirstIndexValueInArray(int[] arr,int indexFrom, int indexTo, int value) {
        for (int i = indexFrom; i < indexTo; i++) {
            if (arr[i] == value){
                return i;
            }
        }
        return -1;
    }
}
