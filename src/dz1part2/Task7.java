package dz1part2;
/*
(1 балл) Петя недавно изучил строки в джаве и решил попрактиковаться с ними.
Ему хочется уметь разделять строку по первому пробелу. Для этого он может
воспользоваться методами indexOf() и substring().
На вход подается строка. Нужно вывести две строки, полученные из входной
разделением по первому пробелу.
Ограничения:
В строке гарантированно есть хотя бы один пробел
Первый и последний символ строки гарантированно не пробел
2 < s.length() < 100
Пример:
Входные данные
Выходные данные
Hi great team! Hi
great team!
Hello world! Hello
world!
 */

import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        System.out.println(s.substring(0, s.indexOf(' ')));
        System.out.println(s.substring(s.indexOf(' ') + 1, s.length()));
    }
}
