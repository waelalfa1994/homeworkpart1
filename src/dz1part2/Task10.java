package dz1part2;
/*
(1 балл) "А логарифмическое?" - не унималась дочь.
Напишите программу, которая проверяет, что log(e^n) == n для любого
вещественного n.
Ограничения:
-500 < n < 500

 */

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double n = scanner.nextDouble();
        System.out.println(Math.log(Math.exp(n))== n);
    }
}
