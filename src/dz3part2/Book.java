package dz3part2;

import java.util.ArrayList;

public class Book {
    private final String name; //название книги
    private final String author;//автор книги

    private int ReaderId;//идентификатор посетителя взявшего книгу, если поле равно нулю значит книга свободна

    private ArrayList<Integer> scoresBook; //список оценок книги посетителями от 1 до 5


    /**
     * Конструктор создает книгу с указанными названием и автором
     * @param name  название
     * @param author автор
     */
    public Book (String name, String author) {
        this.name = name;
        this.author = author;
        this.ReaderId = 0;
        this.scoresBook = new ArrayList<>();
    }

    /**
     * Переопределенный метод для выведения на печать объекта
     * @return строка с полями объекта
     */
    public String toString(){
        return "Название: " + getName() + " Автор: " + getAuthor() + " ID читателя " + getReaderId() + " Средняя оценка: "
                + getAveregeScoresBook() + "\n";
    }

    /**
     * Возвращает название книги
     * @return  название книги
     */
    public String getName () {
        return name;
    }

    /**
     * Возвращает идентификатор посетителя
     * @return идентификатор посетителя
     */
    public int getReaderId () {
        return ReaderId;
    }

    /**
     * Возвращает автора книги
     * @return  автор книги
     */
    public String getAuthor () {
        return author;
    }

    /**
     * Устанавливает идентификатор посетителя после того как он одолжил книгу
     * @param readerId идентификатор посетителя
     */
    public void setReaderId (int readerId) {
        ReaderId = readerId;
    }

    /**
     * Возвращает строку со средней оценкой книги или пустую строку и сообщение, что оценок пока нет
     * @return строка
     */
    public String getAveregeScoresBook () {
        if (scoresBook.isEmpty()){
            System.out.println("Пока оценок еще нет");
            return "";
        } else {
            Integer sum = 0;
            for (Integer i: scoresBook) {
                sum += i;
            }
            double averegeScore = (double) sum / scoresBook.size();
            return String.format("%.2f", averegeScore);
        }
    }

    /**
     * добавляет оценку очередного посетителя
     * @param i оценка книги посетителем
     */
    public void addNewScoresBook (int i) {
        scoresBook.add(i);
    }
}
