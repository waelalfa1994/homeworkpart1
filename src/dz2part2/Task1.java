package dz2part2;
/*
На вход передается N — количество столбцов в двумерном массиве и M —
количество строк. Затем сам передается двумерный массив, состоящий из
натуральных чисел.
Необходимо сохранить в одномерном массиве и вывести на экран
минимальный элемент каждой строки.
Ограничения:
● 0 < N < 100
● 0 < M < 100
● 0 < ai < 1000
 */

import java.util.Scanner;

public class Task1 {
    public static void main (String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = scanner.nextInt();
        int[][] a = new int[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                a[i][j] = scanner.nextInt();
            }
        }
        int[] aMin = new int[m];
        for (int i = 0; i < m; i++) {
            aMin[i] = a[i][0];
            for (int j = 0; j < n; j++) {
                if(a[i][j] < aMin[i]){
                    aMin[i] = a[i][j];
                }
            }
        }
        for (int min: aMin) {
            System.out.print(min + " ");
        }
    }
}
