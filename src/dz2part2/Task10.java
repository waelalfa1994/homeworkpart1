package dz2part2;

import java.util.Arrays;
import java.util.Scanner;

/*
 На вход подается число N. Необходимо вывести цифры числа справа налево.
Решить задачу нужно через рекурсию.
Ограничения:
0 < N < 1000000

 */
public class Task10 {
    public static void main (String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        String s = "" + n;
        char[] chars = s.toCharArray();
        String reversNToString = inversCharsArray(chars);
        char[] chars1 = reversNToString.toCharArray();
        for (int i = 0; i < chars1.length; i++) {
            if(i != chars1.length - 1){
                System.out.print(chars1[i] + " ");
            } else {
                System.out.print(chars1[i]);
            }
        }
    }

    private static String inversCharsArray (char[] chars) {
        if (chars.length == 0){
            return "";
        }
        String s = chars[chars.length - 1] + "";
        char[] newChar = Arrays.copyOf(chars, chars.length - 1);
        return s + inversCharsArray(newChar);
    }
}
