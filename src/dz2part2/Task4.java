package dz2part2;
/*
На вход подается число N — количество строк и столбцов матрицы. Затем
передается сама матрица, состоящая из натуральных чисел. После этого
передается натуральное число P.
Необходимо найти элемент P в матрице и удалить столбец и строку его
содержащий (т.е. сохранить и вывести на экран массив меньшей размерности).
Гарантируется, что искомый элемент единственный в массиве.
Ограничения:
● 0 < N < 100
● 0 < ai < 1000

 */

import java.util.Scanner;

public class Task4 {
    public static void main (String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[][] arr = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
               arr[i][j] = scanner.nextInt();
            }
        }
        int p = scanner.nextInt();
        int[] index = new int[2];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
               if(arr[i][j] == p){
                   index[0] = i;
                   index[1] = j;
               }
            }
        }
        int[][] arr2 = new int[n-1][n-1];
        int countI = 0;
        for (int i = 0; i < n ; i++) {
            int countJ = 0;
            if (i == index[0]) continue;
            for (int j = 0; j < n ; j++) {
                if(j == index[1]) continue;
                arr2[countI][countJ] = arr[i][j];
                countJ++;
            }
            countI++;
        }
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - 1; j++) {
                if (j < n - 2) {
                    System.out.print(arr2[i][j] + " ");
                } else {
                    System.out.print(arr2[i][j]);
                }
            }
            System.out.println();
        }
    }
}
