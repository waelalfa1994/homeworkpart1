package dz2part2;
/*
На вход подается число N. Необходимо посчитать и вывести на экран сумму его
цифр. Решить задачу нужно через рекурсию.
Ограничения:
0 < N < 1000000
 */


import java.util.Arrays;
import java.util.Scanner;

public class Task8 {
    public static void main (String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        String str = "" + n;
        char[] chars = str.toCharArray();
        int summ = summN(chars);

        System.out.println(summ);
    }

    private static int summN (char[] chars) {
        if (chars.length == 0) {
            return 0;
        }
       int summ = chars[chars.length - 1] - '0';
       char[] newChar = Arrays.copyOf(chars, chars.length - 1);

        return summ + summN(newChar);
    }
}
