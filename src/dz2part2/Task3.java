package dz2part2;
/*
На вход подается число N — количество строк и столбцов матрицы. Затем
передаются координаты X и Y расположения коня на шахматной доске.
Необходимо заполнить матрицу размера NxN нулями, местоположение коня
отметить символом K, а позиции, которые он может бить, символом X.
Ограничения:
● 4 < N < 100
● 0 <= X, Y < N
 */

import java.util.Scanner;

public class Task3 {
    public static void main (String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int x = scanner.nextInt();
        int y = scanner.nextInt();
        char[][] arr = new char[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (j == x && i == y){
                    arr[i][j] = 'K';
                } else {
                    arr[i][j] = '0';
                }
            }
        }

        int[][] horseBeats = whereHorseBeats( x, y, n);
        for (int i = 0; i < horseBeats.length; i++) {
            if (horseBeats[i] != null){
                int xHB = horseBeats[i][0];
                int yHB = horseBeats[i][1];

                arr[yHB][xHB] = 'X';
            }
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (j < n - 1){
                    System.out.print(arr[i][j] + " ");
                } else {
                    System.out.print(arr[i][j]);
                }
            }
            System.out.println();
        }
    }

    private static int[][] whereHorseBeats (int x, int y, int n) {
        int[][] arr = new int[8][2];

       if((x + 2 < n) && (x + 2 > 0) && (y + 1 < n && y + 1 > 0)){
           arr[0] = new int[]{x + 2, y + 1};
       } else {
           arr[0] = null;
       }

        if((x + 2 < n) && (x + 2 > 0) && (y - 1 < n && y - 1 > 0)){
            arr[1] = new int[]{x + 2, y - 1};
        } else {
            arr[1] = null;
        }

        if((y + 2 < n) && (y + 2 > 0) && (x - 1 < n && x - 1 > 0)){
            arr[2] = new int[]{x - 1, y + 2};
        } else {
            arr[2] = null;
        }

        if((y + 2 < n) && (y + 2 > 0) && (x + 1 < n && x + 1 > 0)){
            arr[3] = new int[]{x + 1, y + 2};
        } else {
            arr[3] = null;
        }

        if((x - 2 < n) && (x - 2 > 0) && (y + 1 < n && y + 1 > 0)){
            arr[4] = new int[]{x - 2, y + 1};
        } else {
            arr[4] = null;
        }

        if((x - 2 < n) && (x - 2 > 0) && (y - 1 < n && y - 1 > 0)){
            arr[5] = new int[]{x - 2, y - 1};
        } else {
            arr[5] = null;
        }

        if((y - 2 < n) && (y - 2 > 0) && (x - 1 < n && x - 1 > 0)){
            arr[6] = new int[]{x - 1, y - 2};
        } else {
            arr[6] = null;
        }

        if((y - 2 < n) && (y - 2 > 0) && (x + 1 < n && x + 1 > 0)){
            arr[7] = new int[]{x + 1, y - 2};
        } else {
            arr[7] = null;
        }
        return arr;
    }

}
