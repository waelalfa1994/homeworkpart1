package dz2part2;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Scanner;

/*
Раз в год Петя проводит конкурс красоты для собак. К сожалению, система
хранения участников и оценок неудобная, а победителя определить надо. В
первой таблице в системе хранятся имена хозяев, во второй - клички животных,
в третьей — оценки трех судей за выступление каждой собаки. Таблицы
связаны между собой только по индексу. То есть хозяин i-ой собаки указан в i-ой
строке первой таблицы, а ее оценки — в i-ой строке третьей таблицы. Нужно
помочь Пете определить топ 3 победителей конкурса.
На вход подается число N — количество участников конкурса. Затем в N
строках переданы имена хозяев. После этого в N строках переданы клички
собак. Затем передается матрица с N строк, 3 вещественных числа в каждой —
оценки судей. Победителями являются три участника, набравшие
максимальное среднее арифметическое по оценкам 3 судей. Необходимо
вывести трех победителей в формате “Имя хозяина: кличка, средняя оценка”.
Гарантируется, что среднее арифметическое для всех участников будет
различным.
Ограничения:
0 < N < 100
Иван
Николай
Анна
Дарья

Жучка
Кнопка
Цезарь
Добряш

7 6 7
8 8 7
4 5 6
9 9 9
 */
public class Task7 {
    public static void main (String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        String[] arrNameOwners = new String[n];
        for (int i = 0; i < n; i++) {
            arrNameOwners[i] = scanner.next();
        }


        String[] arrDogsNickname = new String[n];
        for (int i = 0; i < n; i++) {
            arrDogsNickname[i] = scanner.next();
        }


        int[][] arrScores = new int[n][3];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < 3; j++) {
                arrScores[i][j] = scanner.nextInt();
            }
        }
        double[] arrScoresArifmMeans = new double[n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < 3; j++) {
               arrScoresArifmMeans[i] += arrScores[i][j];
            }
            arrScoresArifmMeans[i] /= 3;
        }

        int[] arrFirstThreeIndexes = findFirsTreeIndex(arrScoresArifmMeans);


        for (int i = 0; i < arrFirstThreeIndexes.length; i++) {
            System.out.println(arrNameOwners[arrFirstThreeIndexes[i]] + ": " + arrDogsNickname[arrFirstThreeIndexes[i]] +
                    ", " + doubleToString(arrScoresArifmMeans[arrFirstThreeIndexes[i]]) );
        }
    }

    private static String doubleToString (double arrFirstThreeIndex) {

        BigDecimal result = new BigDecimal(arrFirstThreeIndex);
        result = result.setScale(1, RoundingMode.DOWN);
        return result.toString();
    }

    private static int[] findFirsTreeIndex (double[] arrScoresArifmMeans) {

        int[] resArr = new int[3];
        double[] arrTemp = new double[arrScoresArifmMeans.length];
        for (int i = 0; i < arrScoresArifmMeans.length; i++) {
            arrTemp[i] = arrScoresArifmMeans[i];
        }
        Arrays.sort(arrTemp);

        int count = 0;
        for (int i = arrTemp.length - 1; i > arrTemp.length - 4; i--) {
            for (int j = 0; j < arrScoresArifmMeans.length; j++) {
                if(arrTemp[i] == arrScoresArifmMeans[j]){
                    resArr[count] = j;
                    count++;
                }
            }
        }

        return resArr;
    }


}
