package dz2part2;
/*
На вход подается число N — количество строк и столбцов матрицы.
Затем передается сама матрица, состоящая из натуральных чисел.
Необходимо вывести true, если она является симметричной относительно
побочной диагонали, false иначе.
Побочной диагональю называется диагональ, проходящая из верхнего правого
угла в левый нижний.
Ограничения:
● 0 < N < 100
● 0 < ai < 1000
 */

import java.util.Scanner;

public class Task5 {
    public static void main (String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[][] arr = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                arr[i][j] = scanner.nextInt();
            }
        }
        System.out.println(isSimmetrical(arr, 0, arr.length - 1));
    }

    private static boolean isSimmetrical (int[][] arr, int begin, int end) {
        boolean simm = true;

        while (begin < end){
            if (arr[begin][begin] != arr[end][end]){
                return false;
            } else {
                begin++;
                end--;
                simm = isSimmetrical(arr, begin, end);
            }
        }
        return simm;
    }
}
