package pmdz1.Task6;

import java.util.Scanner;

/*
Фронт со своей стороны не сделал обработку входных данных анкеты! Петя
очень зол и ему придется написать свои проверки, а также кидать исключения,
если проверка провалилась. Помогите Пете написать класс FormValidator со
статическими методами проверки. На вход всем методам подается String str.
a. public void checkName(String str) — длина имени должна быть от 2 до 20
символов, первая буква заглавная.
b. public void checkBirthdate(String str) — дата рождения должна быть не
раньше 01.01.1900 и не позже текущей даты.
c. public void checkGender(String str) — пол должен корректно матчится в
enum Gender, хранящий Male и Female значения.
d. public void checkHeight(String str) — рост должен быть положительным
числом и корректно конвертироваться в double.
 */
public class Main {
    public static void main (String[] args) {
        //заполняем форму
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите имя: ");
        String name = scanner.next();
        System.out.println("Введите дату рождения: ");
        String birthdate = scanner.next();
        System.out.println("Введите пол (мужской или женский): ");
        String gender = scanner.next();
        System.out.println("Введите рост: ");
        String height = scanner.next();

        //в статическом методе проверяем форму на возможные ошибки
        checkForm(name, birthdate, gender, height);
    }

    private static void checkForm (String name, String birthdate, String gender, String height) {
        try {
            FormValidator.checkName(name);
            System.out.println("Проверка имени " + name + " завершилась успешно!");

            FormValidator.checkBirthdate(birthdate);
            System.out.println("Проверка даты рождения " + birthdate + " завершилась успешно!");

            FormValidator.checkGender(gender);
            System.out.println("Проверка пола " + gender + " завершилась успешно!");

            FormValidator.checkHeight(height);
            System.out.println("Проверка роста " + height + " завершилась успешно!");

        } catch (FormValidatorException e){
            System.out.println(e.getMessage());
        }catch (NumberFormatException е){
            System.out.println("Число должно корректно конвертироваться в double");
        }
    }


}
