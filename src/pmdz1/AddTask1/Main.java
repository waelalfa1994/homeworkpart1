package pmdz1.AddTask1;
/*
На вход подается число n и массив целых чисел длины n.
Вывести два максимальных числа в этой последовательности.
Пояснение: Вторым максимальным числом считается тот, который окажется
максимальным после вычеркивания первого максимума.
Пример:
Входные данные |  Выходные данные
5
1 3 5 4 5      |     5 5

3
3 2 1          |    3 2

 */

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class Main {
    public static void main (String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите размер массива: ");
        int n = scanner.nextInt();
        int[] arr = new int[n];
        System.out.println("Введите " + n + " значений массива: ");
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
        }

        int[] resArr = popTwoMaxElementArray(arr);

        System.out.println(resArr[0] + " " + resArr[1]);

    }

    /**
     * Метод, который возвращает массив из двух максимальных чисел поданного на вход массива
     * @param arr входной массив
     * @return возвращаемый массив из двух максимальных чисел поданного на вход массива
     */
    private static int[] popTwoMaxElementArray (int[] arr) {
        int[] resultArr = new int[2];
        List<Integer> list = new ArrayList<>();

        for (int i = 0; i < arr.length; i++) {
            list.add(arr[i]);
        }
        resultArr[0] = maxElement(list);
        resultArr[1] = maxElement(list);
        return resultArr;
    }

    /**
     * Метод возвращающий максимальный элемент поданного на вход списка и одновременно удаляющий этот элемент из этого списка
     * @param list  список
     * @return максимальный элемент
     */
    private static int maxElement (List<Integer> list) {
        Integer max = list.get(0);
        int maxIndex = 0;
        for (int i = 1; i < list.size(); i++) {
            if (list.get(i) > max){
                max = list.get(i);
                maxIndex = i;
            }
        }
        list.remove(maxIndex);
        return max.intValue();
    }
}
