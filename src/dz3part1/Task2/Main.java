package dz3part1.Task2;

import java.util.Arrays;

public class Main {
    public static void main (String[] args) {
        Student student = new Student();
        student.setName("Kevin");
        student.setSurname("Durant");
        student.setGrades(new int[]{1, 2, 2, 4, 5, 7, 5, 6, 9, 10, 8, 3});
        student.addNewGrade(6);
        printStudent(student);

    }

    private static void printStudent (Student student) {
        System.out.println("Name: " + student.getName());
        System.out.println("Surname: " + student.getSurname());
        System.out.println("Last 10 grades: " + Arrays.toString(student.getGrades()));
        System.out.println("Arithmetic mean: " + student.arithmeticMean());
    }


}
