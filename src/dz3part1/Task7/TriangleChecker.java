package dz3part1.Task7;
/*
Реализовать класс TriangleChecker, статический метод которого принимает три
длины сторон треугольника и возвращает true, если возможно составить из них
треугольник, иначе false. Входные длины сторон треугольника — числа типа
double. Придумать и написать в методе main несколько тестов для проверки
работоспособности класса (минимум один тест на результат true и один на
результат false)
 */

public class TriangleChecker {

    /**
     * Метод принимает три длины сторон треугольника и возвращает true, если возможно составить из них
     * треугольник, иначе false.
     * @param a сторона
     * @param b сторона
     * @param c сторона
     * @return
     */
    public static boolean checkTriangle(double a, double b, double c){
        if (a + b > c && a + c > b && b + c > a){
            return true;
        }
        return false;
    }
}
