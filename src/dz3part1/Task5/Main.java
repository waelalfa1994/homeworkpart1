package dz3part1.Task5;
/*
Необходимо реализовать класс DayOfWeek для хранения порядкового номера
дня недели (byte) и названия дня недели (String).
Затем в отдельном классе в методе main создать массив объектов DayOfWeek
длины 7. Заполнить его соответствующими значениями (от 1 Monday до 7
Sunday) и вывести значения массива объектов DayOfWeek на экран.
Пример вывода:
1 Monday
2 Tuesday
…
7 Sunday
 */
public class Main {
    public static void main (String[] args) {

        //создаем массив объектов DayOfWeek длины 7 и заполняем массив соответствующими значениями (от 1 Monday до 7 Sunday)
        DayOfWeek[] daysOfWeek = new DayOfWeek[]{
                new DayOfWeek((byte) 1, "Monday"),
                new DayOfWeek((byte) 2, "Tuesday"),
                new DayOfWeek((byte) 3, "Wednesday"),
                new DayOfWeek((byte) 4, "Thursday"),
                new DayOfWeek((byte) 5, "Friday"),
                new DayOfWeek((byte) 6, "Saturday"),
                new DayOfWeek((byte) 7, "Sunday")
        };
        // выводим значения массива объектов DayOfWeek на экран
        for (DayOfWeek day: daysOfWeek) {
            System.out.println(day.getNumberOfDay() + " " + day.getDayOfWeek());
        }

    }
}
