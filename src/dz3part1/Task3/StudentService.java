package dz3part1.Task3;
/*
 Необходимо реализовать класс StudentService.
У класса должны быть реализованы следующие публичные методы:
● bestStudent() — принимает массив студентов (класс Student из
предыдущего задания), возвращает лучшего студента (т.е. который
имеет самый высокий средний балл). Если таких несколько — вывести
любого.
● sortBySurname() — принимает массив студентов (класс Student из
предыдущего задания) и сортирует его по фамилии
 */

import dz3part1.Task2.Student;

public class StudentService {

    /**
     * принимает массив студентов (класс Student из
     * предыдущего задания), возвращает лучшего студента (т.е. который
     * имеет самый высокий средний балл). Если таких несколько — вывести
     * любого.
     * @param students массив студентов
     * @return возвращает лучшего студента
     */
    public static Student bestStudent(Student[] students){
        Student tempBestStudent = students[0];
        for (int i = 0; i < students.length; i++) {
            if (tempBestStudent.arithmeticMean() < students[i].arithmeticMean()){
                tempBestStudent = students[i];
            }
        }
        return tempBestStudent;
    }

    /**
     * принимает массив студентов (класс Student из
     * предыдущего задания) и сортирует его по фамилии
     * @param students массив студентов
     */
    public static void sortBySurname(Student[] students){

        for (int i = 0; i < students.length; i++) {
            Student withFirstSurname = students[i];
            int indexOfwithFirstSurname = 0;
            for (int j = i + 1; j < students.length; j++) {
                if (students[i].getSurname().charAt(0) > students[j].getSurname().charAt(0)){
                    withFirstSurname = students[j];
                    indexOfwithFirstSurname = j;
                }
            }
            if (!students[i].equals(withFirstSurname)){
                students[indexOfwithFirstSurname] = students[i];
                students[i] = withFirstSurname;
            }
        }
    }

}
