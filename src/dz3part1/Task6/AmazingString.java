package dz3part1.Task6;
/*
Необходимо реализовать класс AmazingString, который хранит внутри себя
строку как массив char и предоставляет следующий функционал:
Конструкторы:
● Создание AmazingString, принимая на вход массив char
● Создание AmazingString, принимая на вход String
Публичные методы (названия методов, входные и выходные параметры
продумать самостоятельно). Все методы ниже нужно реализовать “руками”, т.е.
не прибегая к переводу массива char в String и без использования стандартных
методов класса String.
● Вернуть i-ый символ строки
● Вернуть длину строки
● Вывести строку на экран
● Проверить, есть ли переданная подстрока в AmazingString (на вход
подается массив char). Вернуть true, если найдена и false иначе
● Проверить, есть ли переданная подстрока в AmazingString (на вход
подается String). Вернуть true, если найдена и false иначе
● Удалить из строки AmazingString ведущие пробельные символы, если
они есть
● Развернуть строку (первый символ должен стать последним, а
последний первым и т.д.)
 */
public class AmazingString {

    private char[] chars; //массив char

    /**
     * Конструктор принимающий массив char
     * @param chars массив char
     */
    public AmazingString (char[] chars) {
        this.chars = chars;
    }

    /**
     * Конструктор принимающий на вход строку
     * @param s String
     */
    public AmazingString (String s) {
        this.chars = s.toCharArray();
    }

    /**
     *  Взвращает символ строки с заданным индексом
     * @param index индекс
     * @return символ строки с заданным индексом
     */
    public char findToIndex(int index){
        return chars[index];
    }

    /**
     * Возвращает длину строки
     * @return длину строки
     */
    public int length (){
        return chars.length;
    }

    /**
     * Выводит строку на экран
     */
    public void print(){
        for (char c: chars) {
            System.out.print(c);
        }
        System.out.println();
    }

    /**
     * Проверяет, есть ли переданная подстрока в AmazingString (на вход
     * подается массив char). Вернуть true, если найдена и false иначе
     * @param charsSubString подстрока массив char
     * @return  true, если найдена и false иначе
     */
    public boolean isSubString(char[] charsSubString){
        //ищем индексы в массиве chars с первым сиволом подстроки, если таковых нет возвращаем false
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] == charsSubString[0]){
                //проверяем в цикле совпадают ли следующие элементы chars с элементами подстроки
                for (int j = 0; j < charsSubString.length; j++) {
                    if (chars[i + j] != charsSubString[j]){
                        break;
                    } else if (j == charsSubString.length -1){
                        return true;
                    }
                }
            }
        }
        return false;
    }
    /**
     * Проверяет, есть ли переданная подстрока в AmazingString (на вход
     * подается String). Вернуть true, если найдена и false иначе
     * @param subString подстрока  String
     * @return  true, если найдена и false иначе
     */
    public boolean isSubString(String subString){
        char[] chars1 = subString.toCharArray();
        return isSubString(chars1);
    }

    /**
     * Удаляет из строки AmazingString ведущие пробельные символы, если
     * они есть
     * @return строка без пробелов
     */
    public void deleteWhitespace(){
        int counter = 0;
        for (char c: chars) {
            if (c == ' '){
                counter++;
            }
        }
        if (counter > 0){
            char[] newChar = new char[chars.length - counter];
            int newCharIndexes = 0;
            for (int i = 0; i < chars.length; i++) {
                if(chars[i] == ' '){
                    continue;
                } else {
                    newChar[newCharIndexes] = chars[i];
                    newCharIndexes++;
                }
            }
            chars = newChar;
        }
    }


    /**
     * Разврарачивает строку ( т.е. первый символ должен стать последним, а
     * последний первым и т.д.)
     */
    public void reverse(){
        char[] newChar = new char[chars.length];
        int newCharIndexes = 0;
        for (int i = chars.length - 1; i >= 0; i--) {
            newChar[newCharIndexes] = chars[i];
            newCharIndexes++;
        }
        chars = newChar;
    }

}
