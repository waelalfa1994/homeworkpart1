package dz3part1.Task6;

public class Main {
    public static void main (String[] args) {
        AmazingString amazingString = new AmazingString("portison sort best");
        System.out.println(amazingString.findToIndex(4));
        System.out.println(amazingString.length());
        amazingString.print();
        System.out.println(amazingString.isSubString(new char[]{'p','u','r'}));
        System.out.println(amazingString.isSubString(new char[]{'s','o','n'}));
        System.out.println(amazingString.isSubString("ort"));
        amazingString.deleteWhitespace();
        amazingString.print();
        amazingString.reverse();
        amazingString.print();
    }
}
