package dz3part1.Task8;
/*
Реализовать класс “банкомат” Atm.
Класс должен:
● Содержать конструктор, позволяющий задать курс валют перевода
долларов в рубли и курс валют перевода рублей в доллары (можно
выбрать и задать любые положительные значения)
● Содержать два публичных метода, которые позволяют переводить
переданную сумму рублей в доллары и долларов в рубли
● Хранить приватную переменную счетчик — количество созданных
инстансов класса Atm и публичный метод, возвращающий этот счетчик
(подсказка: реализуется через static)
 */

public class Atm {

    private double exchangeRateRubUsd;// курс рубля к американскому доллару
    private static int counter;//счетчик — количество созданных инстансов класса Atm

    /**
     *Конструктор задающий курс валюты, первой по отношению ко второй
     * @param currency1 первая валюта
     * @param exchangeRate курс первой валюты по отношению ко второй
     * @param currency2 вторая валюта
     */
    public Atm (Currency currency1, double exchangeRate, Currency currency2) {
        if (currency1.equals(Currency.USD) && currency2.equals(Currency.RUB)){
            this.exchangeRateRubUsd = 1  / exchangeRate;
        } else if (currency1.equals(Currency.RUB) && currency2.equals(Currency.USD)){
            this.exchangeRateRubUsd = exchangeRate;
        }
        counter++;
    }

    /**
     * Метод получает сумму долларах и переводит ее в рубли по заданному курсу
     * @param amount сумма в долларах
     * @return сумма в рублях
     */
    public double exchangeDollarsToRubles(double amount){
        return amount * exchangeRateRubUsd;
    }

    /**
     * Метод получает сумму в рублях переводит ее в доллары  по заданному курсу
     * @param amount сумма в рублях
     * @return сумма в долларах
     */
    public double exchangeRublesToDollars(double amount){
        return amount / exchangeRateRubUsd;
    }

    /**
     * Метод, возвращающий  количество созданных инстансов класса Atm
     * @return количество созданных инстансов класса Atm
     */
    public static int getCounterInstances(){
        return counter;
    }

}
