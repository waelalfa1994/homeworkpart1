package dz3part1.Task4;

public class Main {
    public static void main (String[] args) {
        TimeUnit time1 = new TimeUnit(13);
        TimeUnit time2 = new TimeUnit(2, 30);
        TimeUnit time3 = new TimeUnit(16,45,30);
        time3.printTimeIn24HoursFormat();
        time3.printTimeIn12HoursFormat();
        time1.printTimeIn12HoursFormat();
        time2.addTime(48, 12,36);
        time2.printTimeIn12HoursFormat();
    }
}
