package dz1part1;

import java.util.Scanner;

/*
Вычислите и выведите на экран объем шара, получив его радиус r с консоли.
Подсказка: считать по формуле V = 4/3 * pi * r^3. Значение числа pi взять из
Math.

 */

public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int r = scanner.nextInt();
        double v = Math.PI * Math.pow(r, 3) * 4 / 3;

        System.out.println("V = " + v);

    }
}
