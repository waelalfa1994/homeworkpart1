package dz1part1;

/*
Hа вход подается количество секунд, прошедших с начала текущего дня – count.
Выведите в консоль текущее время в формате: часы и минуты.
 */

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int s = scanner.nextInt();

        int m = s / 60;
        int h = m / 60;

        System.out.println("Прошло " + h + " часа " + m % 60 + " минут");
    }
}
