
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Scanner;

public class Solution {
    public static void main (String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        String s = "" + n;
        char[] chars = s.toCharArray();
        String reversNToString = inversCharsArray(chars);
        char[] chars1 = reversNToString.toCharArray();
        for (int i = 0; i < chars1.length; i++) {
            if(i != chars1.length - 1){
                System.out.print(chars1[i] + " ");
            } else {
                System.out.print(chars1[i]);
            }
        }
    }

    private static String inversCharsArray (char[] chars) {
        if (chars.length == 0){
            return "";
        }
        String s = chars[0] + "";
        char[] newChar = new char[chars.length - 1];
        for (int i = 1; i < chars.length; i++) {
            newChar[i - 1] = chars[i];
        }
        return s + inversCharsArray(newChar);
    }
}
