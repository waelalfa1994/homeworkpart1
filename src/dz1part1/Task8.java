package dz1part1;

/*
На вход подается баланс счета в банке – n. Рассчитайте дневной бюджет на 30
дней.
Ограничения:
0 < count < 100000
 */

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println((double) n / 30);
    }
}
