package dz1part3;

import java.util.Scanner;

/*
 В банкомате остались только купюры номиналом 8 4 2 1. Дано положительное
число n - количество денег для размена. Необходимо найти минимальное
количество купюр с помощью которых можно разменять это количество денег
(соблюсти порядок: первым числом вывести количество купюр номиналом 8,
вторым - 4 и т д)
Ограничения:
0 < n < 1000000
 */
public class Task6 {
    public static void main(String[] args) {
        int n = new Scanner(System.in).nextInt();
        String s = "";

        for (int i = 8; i > 1; i = i/2){
          s += n / i + " ";
          n %= i;
        }
        s += n;
        System.out.println(s);
    }
}
