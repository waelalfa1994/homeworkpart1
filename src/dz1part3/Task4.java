package dz1part3;

import java.util.Scanner;

/*
    Дано натуральное число n. Вывести его цифры в “столбик”.
    Ограничения:
    0 < n < 1000000
 */
public class Task4 {
    public static void main(String[] args) {
        String s = new Scanner(System.in).next();
        for (int i = 0; i < s.length(); i++){
            System.out.println(s.charAt(i));
        }
    }
}
