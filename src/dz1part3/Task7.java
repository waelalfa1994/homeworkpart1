package dz1part3;

import java.util.Scanner;

/*
 Дана строка s. Вычислить количество символов в ней, не считая пробелов
(необходимо использовать цикл).
Ограничения:
0 < s.length() < 1000
 */
public class Task7 {
    public static void main(String[] args) {
        String s = new Scanner(System.in).nextLine();
        int val = 0;
        for (int i = 0; i < s.length(); i++){
            if(s.charAt(i) != ' '){
                val++;
            }
        }
        System.out.println(val);
    }
}
