package dz1part3;
/*
 Вывести на экран “ёлочку” из символа звездочки (*) заданной высоты N. На N +
1 строке у “ёлочки” должен быть отображен ствол из символа |
Ограничения:
2 < n < 10

 */


import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        int n = new Scanner(System.in).nextInt();

        for (int i = 0; i < n; i++){
            for (int j = 0; j < n - i - 1; j++) {
                System.out.print(" ");
            }
            for (int j = 0; j < i * 2 + 1; j++) {
                System.out.print("#");
            }
            System.out.println("");
        }
        for (int i = 0; i < n * 2 + 1; i++){

            if(i != n - 1){
                System.out.print(" ");
            } else {
                System.out.print("|");
            }
        }
    }
}
