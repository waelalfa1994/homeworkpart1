package dz1part3;
/*
На вход подается:
○ целое число n,
○ целое число p
○ целые числа a1, a2 , … an
Необходимо вычислить сумму всех чисел a1, a2, a3 … an которые строго
больше p.
Ограничения:
0 < m, n, ai < 100
 */

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int p = scanner.nextInt();
        int[] a = new int[n];
        int sum = 0;
        for (int i = 0; i < n; i++){
            a[i] = scanner.nextInt();
            if(a[i] > p){
                sum += a[i];
            }
        }
        System.out.println(sum);
    }
}
