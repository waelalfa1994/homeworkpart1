package dz3part3.Task1;

/**
 *  Конечный класс Золотая рыбка, она является рыбой, поэтому наследуется от класса Fish и плавает поэтому имплементируется
 *   от интерфейса Swimming
 */
public class GoldFish extends Fish
        implements Swimming{

    /**
     *Поле которое отражает скорость плавания животного, в данном случае золотая рыбка плавает медленно
     */
    private String howItSwim = "медленно";

    /**
     * Метод который должны реализовать все плавающие животные
     */
    @Override
    public void swim () {
        System.out.println("Плавает: " + howItSwim);
    }

    public String toString(){
        return "Золотая рыбка";
    }
}
