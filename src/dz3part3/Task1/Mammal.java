package dz3part3.Task1;

/**
 *  Подкласс животных млекопитающие. Все наследующие этот класс животные будут живородящие
 */
public class Mammal extends Animal {

    /**
     * метод выводит на экран как животное рожает: живородящее
     */
    @Override
     public void wayOfBirth () {
        System.out.println("Как животное рожает: живородящее");
    }
}
