package dz3part3.Task2;
/*
Цех по ремонту BestCarpenterEver умеет чинить некоторую Мебель. К сожалению, из Мебели он умеет чинить только
Табуретки, а Столы, например, нет. Реализовать метод в цеху, позволяющий по переданной мебели
определять, сможет ли ей починить или нет. Возвращать результат типа boolean. Протестировать метод.
 */

public class Main {

    public static void main (String[] args) {
        // создаем экземпляр класса цеха по ремонту мебели
        BestCarpenterEver bestCarpenterEver = new BestCarpenterEver();

        //создаем экземпляры мебели как Табуретка и Стол
        Furniture furniture1 = new Stool();
        Furniture furniture2 = new Table();

        //проверяем можем ли цех починить созданные выше экземпляры мебели
        bestCarpenterEver.repair(furniture1);
        bestCarpenterEver.repair(furniture2);
    }


}
