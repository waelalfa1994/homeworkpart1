package dz3part3.Task4;

import java.util.List;

/**
 * Класс участника (хозяина собаки), имплементируется от интерфейса Comparable чтобы участников можно было сравнить между собой
 * При данном вводе участников конкурса (когда они и все поля вводятся строго по порядку), их Id не используется и может показаться,
 * что они не нужны, но я их специально завел и оставил, т.к. в случае изменения Клиента и ввода параметров получится каша
 * и будет не понятно где вообще чья собака
 */
public class Participant
        implements Comparable<Participant>  {
    // идентификационный номер участника
    private int id;

    // имя участника
    private String name;

    // собака участника
    private Dog dog;

    /**
     * Конструктор создающий участника (хозяина собаки) по его номеру и имени
     * @param id  идентификационный номер участника
     * @param name имя участника
     */
    public Participant (int id, String name) {
        this.id = id;
        this.name = name;
    }

    // Геттер возвращающий id участника
    public int getId () {
        return id;
    }

    // Геттер возвращающий имя участника
    public String getName () {
        return name;
    }

    // Геттер возвращающий собаку участника
    public Dog getDog () {
        return dog;
    }

    // Сеттер устанавливающий собаку участника
    public void setDog (Dog dog) {
        this.dog = dog;
    }


    /**
     * Реализует метод compareTo, определенный в Comparable для сравнения двух участников по среднему арифметическому
     * оценок их собак
     */
    @Override
    public int compareTo (Participant participant) {
        if(this.dog.arithmeticMeanScore() > participant.dog.arithmeticMeanScore()){
            return 1;
        } else if (this.dog.arithmeticMeanScore() < participant.dog.arithmeticMeanScore()){
            return -1;
        }
        return 0;
    }
}
