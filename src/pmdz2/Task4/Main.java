package pmdz2.Task4;
/*
В некоторой организации хранятся документы (см. класс Document). Сейчас все
документы лежат в ArrayList, из-за чего поиск по id документа выполняется
неэффективно. Для оптимизации поиска по id, необходимо помочь сотрудникам
перевести хранение документов из ArrayList в HashMap.
public class Document {
public int id;
public String name;
public int pageCount;
}
Реализовать метод со следующей сигнатурой:
public Map<Integer, Document> organizeDocuments(List<Document> documents)

 */

import java.util.*;

public class Main {
    public static void main (String[] args) {
        ArrayList<Document> list = new ArrayList<>();
        list.add(new Document(1, "Book1", 356));
        list.add(new Document(2, "Book2", 335));
        list.add(new Document(3, "Book3", 166));
        list.add(new Document(4, "Book4", 38));
        list.add(new Document(5, "Book5", 432));
        list.add(new Document(6, "Book6", 873));

        for (Map.Entry<Integer, Document> item : organizeDocuments(list).entrySet()){
            System.out.printf("Key: id - %d Value: name - %s, pageCount - %d  \n", item.getKey(), item.getValue().name,
                    item.getValue().pageCount);
        }

    }

    /**
     * Метод, позволяющий перевести хранение документов из ArrayList в HashMap, ключом в данном случае выступает id документа,
     * а значением сам документ
     * @param documents список документов List
     * @return HashMap, ключом в данном случае выступает id документа, а значением сам документ
     */
    public static Map<Integer, Document> organizeDocuments(List<Document> documents){
        Map<Integer, Document> map = new HashMap<>();
        ListIterator<Document> listIterator = documents.listIterator();
        while (listIterator.hasNext()){
            Document doc = listIterator.next();
            map.put(doc.id, doc);
        }
        return map;
    }
}
