package pmdz2.Task3;

import java.util.HashSet;
import java.util.Set;

public class PowerfulSet {

    /**
     * Метод, который возвращает пересечение двух наборов
     * @param set1 первый набор
     * @param set2 второй набор
     * @param <T> тип значений наборов
     * @return набор получаемый путем пересечения двух наборов
     */
    public <T> Set<T> intersection(Set<T> set1, Set<T> set2){
        HashSet<T> hashSet = new HashSet<>(set1);
        hashSet.retainAll(set2);
        return hashSet;
    }

    /**
     * Метод, который возвращает объединение двух наборов
     * @param set1 первый набор
     * @param set2 второй набор
     * @param <T> тип значений наборов
     * @return набор получаемый путем объединения двух наборов
     */
    public <T> Set<T> union(Set<T> set1, Set<T> set2){
        HashSet<T> hashSet = new HashSet<>(set1);
        hashSet.addAll(set2);
        return hashSet;
    }

    /**
     * Метод возвращающий элементы первого набора без тех, которые находятся также и во втором наборе
     * @param set1 первый набор
     * @param set2 второй набор
     * @param <T> тип значений наборов
     * @return набор получаемый путем возвращения элементов первого набора без тех, которые находятся также и во втором наборе
     */
    public <T> Set<T> relativeComplement(Set<T> set1, Set<T> set2){
        HashSet<T> hashSet = new HashSet<>(set1);
        hashSet.removeAll(set2);
        return hashSet;
    }
}
