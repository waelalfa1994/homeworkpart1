package pmdz2.Task3;
/*
Реализовать класс PowerfulSet, в котором должны быть следующие методы:
a. public <T> Set<T> intersection(Set<T> set1, Set<T> set2) — возвращает
пересечение двух наборов. Пример: set1 = {1, 2, 3}, set2 = {0, 1, 2, 4}.
Вернуть {1, 2}
b. public <T> Set<T> union(Set<T> set1, Set<T> set2) — возвращает
объединение двух наборов. Пример: set1 = {1, 2, 3}, set2 = {0, 1, 2, 4}.
Вернуть {0, 1, 2, 3, 4}
c. public <T> Set<T> relativeComplement(Set<T> set1, Set<T> set2) —
возвращает элементы первого набора без тех, которые находятся также
и во втором наборе. Пример: set1 = {1, 2, 3}, set2 = {0, 1, 2, 4}. Вернуть {3}
 */

import java.util.HashSet;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Set;

public class Main {
    public static void main (String[] args) {
        PowerfulSet powerfulSet = new PowerfulSet();
        Set<Integer> set1 = new HashSet<>();
        set1.add(1);
        set1.add(2);
        set1.add(3);
        System.out.print("set1 = ");
        printSet(set1);
        Set<Integer> set2 = new HashSet<>();
        set2.add(0);
        set2.add(1);
        set2.add(2);
        set2.add(4);
        System.out.print("set2 = ");
        printSet(set2);

        Set<Integer> result = powerfulSet.intersection(set1,set2);
        printSet(result);

        result = powerfulSet.union(set1, set2);
        printSet(result);

        result = powerfulSet.relativeComplement(set1, set2);
        printSet(result);

    }

    private static <T> void printSet (Set<T> set1) {
        LinkedList<T> list = new LinkedList<>(set1);
        ListIterator<T> listIterator = list.listIterator();
        System.out.print("{");
        while (listIterator.hasNext()){
            System.out.print(listIterator.next());
            if (listIterator.hasNext()){
                System.out.print(", ");
            }
        }
        System.out.println("}");
    }


}
