package pmdz2.AddTask;
/*
Реализовать метод, который принимает массив words и целое положительное число k.
Необходимо вернуть k наиболее часто встречающихся слов..
Результирующий массив должен быть отсортирован по убыванию частоты
встречаемого слова. В случае одинакового количества частоты для слов, то
отсортировать и выводить их по убыванию в лексикографическом порядке.
Входные данные
words =
["the","day","is","sunny","the","the","the",
"sunny","is","is","day"]
k = 4
Выходные данные
["the","is","sunny","day"]
 */

import java.util.HashMap;
import java.util.Map;


public class Main {
    public static void main (String[] args) {

        String[] words = new String[]{"the","day","is","sunny","the","the","the",
                "sunny","is","is","day","is"};
        int k = 4;
        String[] resultArray = findSortedArray(words, k);
        for (int i = 0; i < k; i++) {
            if(i == 0){
                System.out.print("[\"" + resultArray[i] + "\", ");

            } else if (i == k - 1){
                System.out.print("\"" + resultArray[i] + "\"]");
            }else {
                System.out.print("\"" + resultArray[i] + "\", ");
            }
        }
    }

    /**
     * Метод возвращающий результирующий массив, отсортированный по убыванию частоты встречаемого слова в переданном на вход массиве
     * @param words переданный на вход массив
     * @param k длина возвращаемого массива
     * @return результирующий массив, отсортированный по убыванию частоты встречаемого слова в переданном на вход массиве
     */
    private static String[] findSortedArray (String[] words, int k) {
        Map<String, Integer> map = new HashMap<>();
        String[] resultWords = new String[k];
        for (String word: words) {
            if(map.isEmpty() || !map.containsKey(word)){
                map.put(word, 1);
            } else {
                int numberOfRepetitions = map.get(word);
                map.put(word,++numberOfRepetitions);
            }
        }
        int counter = 0;
        while (!map.isEmpty() && counter < k){
                Map.Entry<String, Integer> item = findMaxValueEntry(map);
                resultWords[counter] = item.getKey();
                map.remove(item.getKey());
                counter++;
        }

        return resultWords;
    }

    /**
     * Метод возвращающий элемент коллекции Map с наибольшим значением, в случае если значения равны, то он возвращает тот
     * элемент, который будет первым при сортировке ключей по их убыванию в лексикографическом порядке
     * @param map подаваемая на вход коллекция Map
     * @return элемент коллекции Map.Entry с наибольшим значением
     */
    private static Map.Entry<String, Integer> findMaxValueEntry (Map<String, Integer> map) {

        Map.Entry<String, Integer> maxValueEntry = null;

        for (Map.Entry<String, Integer> item : map.entrySet()){
            try {
                if(maxValueEntry.getValue() < item.getValue() || (maxValueEntry.getValue() == item.getValue()
                        && compareString(maxValueEntry.getKey(), item.getKey()))){
                    maxValueEntry = item;
                }
            } catch (NullPointerException e){
                maxValueEntry = item;
            }

        }
        return maxValueEntry;
    }

    /**
     *  Метод сравнивающий две строки по их убыванию в лексикографическом порядке
     * @param key первая строка
     * @param key1 вторая строка
     * @return значение boolean
     */
    private static boolean compareString (String key, String key1) {
        for (int i = 0; i < key.length() - 1; i++) {
            if(key.charAt(i) < key1.charAt(i)){
                return true;
            } else if (key.charAt(i) > key1.charAt(i)) {
                return false;
            }
        }
        return false;
    }

}


